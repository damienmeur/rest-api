<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_f4900355f6937d27132192a500e3acdf0515cc324959675fa460fa052ccc39d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e7d90cd8b1d66e4b171825e0ac51f5c2985275b179f2d640293481daacfb11fb = $this->env->getExtension("native_profiler");
        $__internal_e7d90cd8b1d66e4b171825e0ac51f5c2985275b179f2d640293481daacfb11fb->enter($__internal_e7d90cd8b1d66e4b171825e0ac51f5c2985275b179f2d640293481daacfb11fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_e7d90cd8b1d66e4b171825e0ac51f5c2985275b179f2d640293481daacfb11fb->leave($__internal_e7d90cd8b1d66e4b171825e0ac51f5c2985275b179f2d640293481daacfb11fb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
