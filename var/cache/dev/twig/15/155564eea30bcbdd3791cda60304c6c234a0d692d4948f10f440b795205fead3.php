<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_514d2993e588fabfb75f6b188fccd24d0fb7e5a81c17ca118fce293e3b574e95 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2aa4bd90f4cf153542d9edce38c0b6f72a05a1bd5e3d1e737403d170917e8879 = $this->env->getExtension("native_profiler");
        $__internal_2aa4bd90f4cf153542d9edce38c0b6f72a05a1bd5e3d1e737403d170917e8879->enter($__internal_2aa4bd90f4cf153542d9edce38c0b6f72a05a1bd5e3d1e737403d170917e8879_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_2aa4bd90f4cf153542d9edce38c0b6f72a05a1bd5e3d1e737403d170917e8879->leave($__internal_2aa4bd90f4cf153542d9edce38c0b6f72a05a1bd5e3d1e737403d170917e8879_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
