<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_0b4349d780dfa5c2920d5628bbb8cf9c52e90ce39d661c98e13aa822655c37fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_63c473ff42ff3b2be7dba166e397056f4131bd389d9f0cc61e7e50791e21b9cd = $this->env->getExtension("native_profiler");
        $__internal_63c473ff42ff3b2be7dba166e397056f4131bd389d9f0cc61e7e50791e21b9cd->enter($__internal_63c473ff42ff3b2be7dba166e397056f4131bd389d9f0cc61e7e50791e21b9cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_63c473ff42ff3b2be7dba166e397056f4131bd389d9f0cc61e7e50791e21b9cd->leave($__internal_63c473ff42ff3b2be7dba166e397056f4131bd389d9f0cc61e7e50791e21b9cd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
