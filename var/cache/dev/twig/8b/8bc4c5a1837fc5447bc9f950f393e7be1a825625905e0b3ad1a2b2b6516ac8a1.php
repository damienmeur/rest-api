<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_07a871ea05aa4d11287dde684365b1f085539bd98390d8644048886a52bbca06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_111ca7c8cc3313eb92fce660f042b31a01c71351bf0e4446578798b7bea4edc5 = $this->env->getExtension("native_profiler");
        $__internal_111ca7c8cc3313eb92fce660f042b31a01c71351bf0e4446578798b7bea4edc5->enter($__internal_111ca7c8cc3313eb92fce660f042b31a01c71351bf0e4446578798b7bea4edc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_111ca7c8cc3313eb92fce660f042b31a01c71351bf0e4446578798b7bea4edc5->leave($__internal_111ca7c8cc3313eb92fce660f042b31a01c71351bf0e4446578798b7bea4edc5_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_cef475a49e6e02b79c08db64cf2f1e1fb1cb4b8dab906b703d6dc4073c60887d = $this->env->getExtension("native_profiler");
        $__internal_cef475a49e6e02b79c08db64cf2f1e1fb1cb4b8dab906b703d6dc4073c60887d->enter($__internal_cef475a49e6e02b79c08db64cf2f1e1fb1cb4b8dab906b703d6dc4073c60887d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_cef475a49e6e02b79c08db64cf2f1e1fb1cb4b8dab906b703d6dc4073c60887d->leave($__internal_cef475a49e6e02b79c08db64cf2f1e1fb1cb4b8dab906b703d6dc4073c60887d_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_ad37ad819374240cd6b8018e7bf0c45ecdfa5b03cf4ad75cf4913284b0ed3641 = $this->env->getExtension("native_profiler");
        $__internal_ad37ad819374240cd6b8018e7bf0c45ecdfa5b03cf4ad75cf4913284b0ed3641->enter($__internal_ad37ad819374240cd6b8018e7bf0c45ecdfa5b03cf4ad75cf4913284b0ed3641_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_ad37ad819374240cd6b8018e7bf0c45ecdfa5b03cf4ad75cf4913284b0ed3641->leave($__internal_ad37ad819374240cd6b8018e7bf0c45ecdfa5b03cf4ad75cf4913284b0ed3641_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_ce494ac9b688d3be9283743d4c007a3e83d15dae28b4186c3da5686c0bf04670 = $this->env->getExtension("native_profiler");
        $__internal_ce494ac9b688d3be9283743d4c007a3e83d15dae28b4186c3da5686c0bf04670->enter($__internal_ce494ac9b688d3be9283743d4c007a3e83d15dae28b4186c3da5686c0bf04670_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_ce494ac9b688d3be9283743d4c007a3e83d15dae28b4186c3da5686c0bf04670->leave($__internal_ce494ac9b688d3be9283743d4c007a3e83d15dae28b4186c3da5686c0bf04670_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
