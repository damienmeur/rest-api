<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_332b36d120e58a1363200079d1a8cfb5cdc3eb666b3c4264ce6da323087bcde6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fece2bcffb3fd7cf3aa1dc01686a2a0e5383bd79464ac71cc89d91cfba4c2144 = $this->env->getExtension("native_profiler");
        $__internal_fece2bcffb3fd7cf3aa1dc01686a2a0e5383bd79464ac71cc89d91cfba4c2144->enter($__internal_fece2bcffb3fd7cf3aa1dc01686a2a0e5383bd79464ac71cc89d91cfba4c2144_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_fece2bcffb3fd7cf3aa1dc01686a2a0e5383bd79464ac71cc89d91cfba4c2144->leave($__internal_fece2bcffb3fd7cf3aa1dc01686a2a0e5383bd79464ac71cc89d91cfba4c2144_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
