<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_2b3a02fce35be870fd01d3ee6cce3ef07a837df640ab115cf4ac1eda024b9da8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e33229aa3d8da63d4f6027c74b9f1a677fcbbdf6181e82394bf965642af8f4db = $this->env->getExtension("native_profiler");
        $__internal_e33229aa3d8da63d4f6027c74b9f1a677fcbbdf6181e82394bf965642af8f4db->enter($__internal_e33229aa3d8da63d4f6027c74b9f1a677fcbbdf6181e82394bf965642af8f4db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_e33229aa3d8da63d4f6027c74b9f1a677fcbbdf6181e82394bf965642af8f4db->leave($__internal_e33229aa3d8da63d4f6027c74b9f1a677fcbbdf6181e82394bf965642af8f4db_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
