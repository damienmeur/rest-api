<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_1a64e472fcff68cf7aa8c9c7e8c3f4597f6c86f9e537f57719aeb504bb7c1e52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cef411a48a9af9c7333f3168326d1d44e97e11d11e36c929ba9e2f23c46d6b0b = $this->env->getExtension("native_profiler");
        $__internal_cef411a48a9af9c7333f3168326d1d44e97e11d11e36c929ba9e2f23c46d6b0b->enter($__internal_cef411a48a9af9c7333f3168326d1d44e97e11d11e36c929ba9e2f23c46d6b0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_cef411a48a9af9c7333f3168326d1d44e97e11d11e36c929ba9e2f23c46d6b0b->leave($__internal_cef411a48a9af9c7333f3168326d1d44e97e11d11e36c929ba9e2f23c46d6b0b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
