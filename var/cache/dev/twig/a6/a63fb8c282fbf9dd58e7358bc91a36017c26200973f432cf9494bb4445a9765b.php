<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_640468356746f909dc94ede37261224b7b2fb44671a2668233abbdf71f1a73e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_76c883f3ccdd9c8ab43cfb19092e716655057b86de154cafce8e8a05e4cfe9f4 = $this->env->getExtension("native_profiler");
        $__internal_76c883f3ccdd9c8ab43cfb19092e716655057b86de154cafce8e8a05e4cfe9f4->enter($__internal_76c883f3ccdd9c8ab43cfb19092e716655057b86de154cafce8e8a05e4cfe9f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_76c883f3ccdd9c8ab43cfb19092e716655057b86de154cafce8e8a05e4cfe9f4->leave($__internal_76c883f3ccdd9c8ab43cfb19092e716655057b86de154cafce8e8a05e4cfe9f4_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
