<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_b7631112fe84d643da3cfe297e55248b55617b3c2086c3b366883b4f67213323 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_111f7495eb76e2419d01264521549df088fed9ac27de189ebcfb987594b49f6b = $this->env->getExtension("native_profiler");
        $__internal_111f7495eb76e2419d01264521549df088fed9ac27de189ebcfb987594b49f6b->enter($__internal_111f7495eb76e2419d01264521549df088fed9ac27de189ebcfb987594b49f6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_111f7495eb76e2419d01264521549df088fed9ac27de189ebcfb987594b49f6b->leave($__internal_111f7495eb76e2419d01264521549df088fed9ac27de189ebcfb987594b49f6b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
