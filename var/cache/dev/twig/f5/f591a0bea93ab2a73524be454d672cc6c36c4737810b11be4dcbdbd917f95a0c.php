<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_2a367b70485cbccf91e87c3ba8637062385652c637c159c4de88031f10d30870 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_38c81b63edf9bcd95ebdc3c0506e581cca0270482806cdd74647522d64427a33 = $this->env->getExtension("native_profiler");
        $__internal_38c81b63edf9bcd95ebdc3c0506e581cca0270482806cdd74647522d64427a33->enter($__internal_38c81b63edf9bcd95ebdc3c0506e581cca0270482806cdd74647522d64427a33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_38c81b63edf9bcd95ebdc3c0506e581cca0270482806cdd74647522d64427a33->leave($__internal_38c81b63edf9bcd95ebdc3c0506e581cca0270482806cdd74647522d64427a33_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
