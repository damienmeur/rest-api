<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_e282a2c744d4fd332db0925590bb2907f51572af8da2cea6ee3caf07a90412df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5377db40d27d67cac1aba4d99cfe7e10682d03289d4782ddbe06ef7cf5e41071 = $this->env->getExtension("native_profiler");
        $__internal_5377db40d27d67cac1aba4d99cfe7e10682d03289d4782ddbe06ef7cf5e41071->enter($__internal_5377db40d27d67cac1aba4d99cfe7e10682d03289d4782ddbe06ef7cf5e41071_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_5377db40d27d67cac1aba4d99cfe7e10682d03289d4782ddbe06ef7cf5e41071->leave($__internal_5377db40d27d67cac1aba4d99cfe7e10682d03289d4782ddbe06ef7cf5e41071_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
