<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_dc9b29871ae77736d63cdd35eb5566b5660d8246ea2fec960759e882bcb6f1a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f11f49365ddb677e389f73acec267b172437798a95cd2bb56f1420e68319337 = $this->env->getExtension("native_profiler");
        $__internal_4f11f49365ddb677e389f73acec267b172437798a95cd2bb56f1420e68319337->enter($__internal_4f11f49365ddb677e389f73acec267b172437798a95cd2bb56f1420e68319337_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_4f11f49365ddb677e389f73acec267b172437798a95cd2bb56f1420e68319337->leave($__internal_4f11f49365ddb677e389f73acec267b172437798a95cd2bb56f1420e68319337_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_eac702719b50b6ce683a5923772e0ad55c259479c0620e69a2e545f26c0c3b4c = $this->env->getExtension("native_profiler");
        $__internal_eac702719b50b6ce683a5923772e0ad55c259479c0620e69a2e545f26c0c3b4c->enter($__internal_eac702719b50b6ce683a5923772e0ad55c259479c0620e69a2e545f26c0c3b4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_eac702719b50b6ce683a5923772e0ad55c259479c0620e69a2e545f26c0c3b4c->leave($__internal_eac702719b50b6ce683a5923772e0ad55c259479c0620e69a2e545f26c0c3b4c_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
