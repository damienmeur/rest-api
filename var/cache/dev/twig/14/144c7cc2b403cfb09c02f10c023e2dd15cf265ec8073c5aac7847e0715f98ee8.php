<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_445e7d8d594a7793e60bff0e5b417a80402a730b1f16b95ef4e626d7f2a57a85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5dca63ddd114819278510c239a4929fb0c2a8926d163bbdaa092a2b9b6116b93 = $this->env->getExtension("native_profiler");
        $__internal_5dca63ddd114819278510c239a4929fb0c2a8926d163bbdaa092a2b9b6116b93->enter($__internal_5dca63ddd114819278510c239a4929fb0c2a8926d163bbdaa092a2b9b6116b93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_5dca63ddd114819278510c239a4929fb0c2a8926d163bbdaa092a2b9b6116b93->leave($__internal_5dca63ddd114819278510c239a4929fb0c2a8926d163bbdaa092a2b9b6116b93_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
