<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_ed6a8c562e19209953867ff26378155cbce479098ef27481ae60562e23019a38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09006bc5010b729751939d9fa21f5d259cf6ae27ec73cf1a6593de8b32186b03 = $this->env->getExtension("native_profiler");
        $__internal_09006bc5010b729751939d9fa21f5d259cf6ae27ec73cf1a6593de8b32186b03->enter($__internal_09006bc5010b729751939d9fa21f5d259cf6ae27ec73cf1a6593de8b32186b03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_09006bc5010b729751939d9fa21f5d259cf6ae27ec73cf1a6593de8b32186b03->leave($__internal_09006bc5010b729751939d9fa21f5d259cf6ae27ec73cf1a6593de8b32186b03_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
