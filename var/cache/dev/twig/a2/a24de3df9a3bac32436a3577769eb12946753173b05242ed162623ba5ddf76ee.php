<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_5cf593f528bd23a6b18bbeee5f4f8c93158318eaade25c82f424194fb02d5c4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6520c6b93611251d75999436474b697b0a96a0a3a562720461b3534d35bca99d = $this->env->getExtension("native_profiler");
        $__internal_6520c6b93611251d75999436474b697b0a96a0a3a562720461b3534d35bca99d->enter($__internal_6520c6b93611251d75999436474b697b0a96a0a3a562720461b3534d35bca99d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_6520c6b93611251d75999436474b697b0a96a0a3a562720461b3534d35bca99d->leave($__internal_6520c6b93611251d75999436474b697b0a96a0a3a562720461b3534d35bca99d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
