<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_a197cbc5d118971459fa6cc200e8a797cbe4dbee5ea06b867c34d471e7db99f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d1b28dcc0b56188c1e83f755230fb999df1501a6945a870079642d73ef1cf54c = $this->env->getExtension("native_profiler");
        $__internal_d1b28dcc0b56188c1e83f755230fb999df1501a6945a870079642d73ef1cf54c->enter($__internal_d1b28dcc0b56188c1e83f755230fb999df1501a6945a870079642d73ef1cf54c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_d1b28dcc0b56188c1e83f755230fb999df1501a6945a870079642d73ef1cf54c->leave($__internal_d1b28dcc0b56188c1e83f755230fb999df1501a6945a870079642d73ef1cf54c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
