<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_b72ff72d55ede9ee57cb78f92d5abba06b0421bbde365422de359551d72adae3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3fa626043856fd06958cecc23b13c337d214f8c9ff4d6a0be801e80a2a417787 = $this->env->getExtension("native_profiler");
        $__internal_3fa626043856fd06958cecc23b13c337d214f8c9ff4d6a0be801e80a2a417787->enter($__internal_3fa626043856fd06958cecc23b13c337d214f8c9ff4d6a0be801e80a2a417787_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_3fa626043856fd06958cecc23b13c337d214f8c9ff4d6a0be801e80a2a417787->leave($__internal_3fa626043856fd06958cecc23b13c337d214f8c9ff4d6a0be801e80a2a417787_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
