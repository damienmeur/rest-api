<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_d7c90241c35f452c993430aa6dd04055ecf156482834b0eabfe18daa4a1b7571 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a617471d291ea8b2b6cd28cf49fa43dc4e6260212b2a51c3c42f20da9c29ca2 = $this->env->getExtension("native_profiler");
        $__internal_0a617471d291ea8b2b6cd28cf49fa43dc4e6260212b2a51c3c42f20da9c29ca2->enter($__internal_0a617471d291ea8b2b6cd28cf49fa43dc4e6260212b2a51c3c42f20da9c29ca2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_0a617471d291ea8b2b6cd28cf49fa43dc4e6260212b2a51c3c42f20da9c29ca2->leave($__internal_0a617471d291ea8b2b6cd28cf49fa43dc4e6260212b2a51c3c42f20da9c29ca2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
