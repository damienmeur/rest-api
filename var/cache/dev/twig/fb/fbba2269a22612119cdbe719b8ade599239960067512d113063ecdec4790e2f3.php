<?php

/* @Twig/Exception/error.atom.twig */
class __TwigTemplate_797af8764663d1fe6faf58a37dfa033173f9ac2d2b45e2a2465bf3e03146ede0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56a8a539e2efb51b25c71931b96c364b85385d02340be3fe766b08360044caee = $this->env->getExtension("native_profiler");
        $__internal_56a8a539e2efb51b25c71931b96c364b85385d02340be3fe766b08360044caee->enter($__internal_56a8a539e2efb51b25c71931b96c364b85385d02340be3fe766b08360044caee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "@Twig/Exception/error.atom.twig", 1)->display($context);
        
        $__internal_56a8a539e2efb51b25c71931b96c364b85385d02340be3fe766b08360044caee->leave($__internal_56a8a539e2efb51b25c71931b96c364b85385d02340be3fe766b08360044caee_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
