<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_418113a8bdb303ee53dd14c84ab370092737064f64b75e940764bffa5b2e7f8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b6283d3a090f1e0de0afd3842ced03942aeb3d2837c53c62794d04f726bfef89 = $this->env->getExtension("native_profiler");
        $__internal_b6283d3a090f1e0de0afd3842ced03942aeb3d2837c53c62794d04f726bfef89->enter($__internal_b6283d3a090f1e0de0afd3842ced03942aeb3d2837c53c62794d04f726bfef89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_b6283d3a090f1e0de0afd3842ced03942aeb3d2837c53c62794d04f726bfef89->leave($__internal_b6283d3a090f1e0de0afd3842ced03942aeb3d2837c53c62794d04f726bfef89_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
