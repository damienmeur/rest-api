<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_d19637f77190c7303921ed4d70dd90d14604f8646d3254f34442fbaee9c9a255 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3da4a07c91e578677e8380a5d20664d534140c93acdb5b0be0ce0a44d0f9a27b = $this->env->getExtension("native_profiler");
        $__internal_3da4a07c91e578677e8380a5d20664d534140c93acdb5b0be0ce0a44d0f9a27b->enter($__internal_3da4a07c91e578677e8380a5d20664d534140c93acdb5b0be0ce0a44d0f9a27b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_3da4a07c91e578677e8380a5d20664d534140c93acdb5b0be0ce0a44d0f9a27b->leave($__internal_3da4a07c91e578677e8380a5d20664d534140c93acdb5b0be0ce0a44d0f9a27b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
