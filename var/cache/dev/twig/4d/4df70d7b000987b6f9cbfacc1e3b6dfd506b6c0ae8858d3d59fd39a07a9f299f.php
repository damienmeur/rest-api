<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_7c8762b1ad3c7650e32d919f9ae34cd6656d75458958bd5cdb32d013a9147bd6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee6fcca9d7d557b194bf24d16eca67d16ef2914e07dab94f46366ca149d0152c = $this->env->getExtension("native_profiler");
        $__internal_ee6fcca9d7d557b194bf24d16eca67d16ef2914e07dab94f46366ca149d0152c->enter($__internal_ee6fcca9d7d557b194bf24d16eca67d16ef2914e07dab94f46366ca149d0152c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ee6fcca9d7d557b194bf24d16eca67d16ef2914e07dab94f46366ca149d0152c->leave($__internal_ee6fcca9d7d557b194bf24d16eca67d16ef2914e07dab94f46366ca149d0152c_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_d7da6ef41768feab5cb4cd65a968a112db3c976c6b57c80c52b7430efc45b75a = $this->env->getExtension("native_profiler");
        $__internal_d7da6ef41768feab5cb4cd65a968a112db3c976c6b57c80c52b7430efc45b75a->enter($__internal_d7da6ef41768feab5cb4cd65a968a112db3c976c6b57c80c52b7430efc45b75a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_d7da6ef41768feab5cb4cd65a968a112db3c976c6b57c80c52b7430efc45b75a->leave($__internal_d7da6ef41768feab5cb4cd65a968a112db3c976c6b57c80c52b7430efc45b75a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_51372e3e2ff0248b29e78865caa73dfab510991e26ec7aea935cad28785821c0 = $this->env->getExtension("native_profiler");
        $__internal_51372e3e2ff0248b29e78865caa73dfab510991e26ec7aea935cad28785821c0->enter($__internal_51372e3e2ff0248b29e78865caa73dfab510991e26ec7aea935cad28785821c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_51372e3e2ff0248b29e78865caa73dfab510991e26ec7aea935cad28785821c0->leave($__internal_51372e3e2ff0248b29e78865caa73dfab510991e26ec7aea935cad28785821c0_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_f382be25bc5233c9030dda6dc2cc78a3fa7caf4887db0356d3adaef295be4278 = $this->env->getExtension("native_profiler");
        $__internal_f382be25bc5233c9030dda6dc2cc78a3fa7caf4887db0356d3adaef295be4278->enter($__internal_f382be25bc5233c9030dda6dc2cc78a3fa7caf4887db0356d3adaef295be4278_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_f382be25bc5233c9030dda6dc2cc78a3fa7caf4887db0356d3adaef295be4278->leave($__internal_f382be25bc5233c9030dda6dc2cc78a3fa7caf4887db0356d3adaef295be4278_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
