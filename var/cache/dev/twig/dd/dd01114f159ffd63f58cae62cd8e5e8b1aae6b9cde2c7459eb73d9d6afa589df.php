<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_382913938d39d4214da63858931bb935e79d5193ea0bc7c888cba99fa87d38f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b3d50edbc1464148c2e9f840bb9d4479e3e000141e7dbbf974711cf6377aa21 = $this->env->getExtension("native_profiler");
        $__internal_6b3d50edbc1464148c2e9f840bb9d4479e3e000141e7dbbf974711cf6377aa21->enter($__internal_6b3d50edbc1464148c2e9f840bb9d4479e3e000141e7dbbf974711cf6377aa21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_6b3d50edbc1464148c2e9f840bb9d4479e3e000141e7dbbf974711cf6377aa21->leave($__internal_6b3d50edbc1464148c2e9f840bb9d4479e3e000141e7dbbf974711cf6377aa21_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_f9783a3624ca3338265d4a4a6c201fcaca93eef9afcf166ab92c2f3c499bdac8 = $this->env->getExtension("native_profiler");
        $__internal_f9783a3624ca3338265d4a4a6c201fcaca93eef9afcf166ab92c2f3c499bdac8->enter($__internal_f9783a3624ca3338265d4a4a6c201fcaca93eef9afcf166ab92c2f3c499bdac8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_f9783a3624ca3338265d4a4a6c201fcaca93eef9afcf166ab92c2f3c499bdac8->leave($__internal_f9783a3624ca3338265d4a4a6c201fcaca93eef9afcf166ab92c2f3c499bdac8_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
