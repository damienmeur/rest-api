<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_52dfea37e86182856966567be1bfc48f52f25fb2e3f52589b34e5e9385ac5a07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9535cc7bd9064b348f31e73fbc173b26ca982debc1b4f79dd11dbdc5bbd5be25 = $this->env->getExtension("native_profiler");
        $__internal_9535cc7bd9064b348f31e73fbc173b26ca982debc1b4f79dd11dbdc5bbd5be25->enter($__internal_9535cc7bd9064b348f31e73fbc173b26ca982debc1b4f79dd11dbdc5bbd5be25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_9535cc7bd9064b348f31e73fbc173b26ca982debc1b4f79dd11dbdc5bbd5be25->leave($__internal_9535cc7bd9064b348f31e73fbc173b26ca982debc1b4f79dd11dbdc5bbd5be25_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
