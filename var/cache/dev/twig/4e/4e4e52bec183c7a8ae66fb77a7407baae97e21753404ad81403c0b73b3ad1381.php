<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_afa45b10a3bcfa306bccd27ea69b3432d804572071e848229cabf66a08b58fad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c1c1ca1f262dd8da030dae670b7474001ddf8045e8ade76296cb7e140c48f1f4 = $this->env->getExtension("native_profiler");
        $__internal_c1c1ca1f262dd8da030dae670b7474001ddf8045e8ade76296cb7e140c48f1f4->enter($__internal_c1c1ca1f262dd8da030dae670b7474001ddf8045e8ade76296cb7e140c48f1f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_c1c1ca1f262dd8da030dae670b7474001ddf8045e8ade76296cb7e140c48f1f4->leave($__internal_c1c1ca1f262dd8da030dae670b7474001ddf8045e8ade76296cb7e140c48f1f4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
