<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_10a8e94dd719a5c21328b888d6f2eaee2003ba6c4a6b7b427932eb22f4fdd966 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_932a981b6d6cbfb309d7a3632b1f5575ae1e6dc21a9ba6b0cbe47a20e7133de6 = $this->env->getExtension("native_profiler");
        $__internal_932a981b6d6cbfb309d7a3632b1f5575ae1e6dc21a9ba6b0cbe47a20e7133de6->enter($__internal_932a981b6d6cbfb309d7a3632b1f5575ae1e6dc21a9ba6b0cbe47a20e7133de6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_932a981b6d6cbfb309d7a3632b1f5575ae1e6dc21a9ba6b0cbe47a20e7133de6->leave($__internal_932a981b6d6cbfb309d7a3632b1f5575ae1e6dc21a9ba6b0cbe47a20e7133de6_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
