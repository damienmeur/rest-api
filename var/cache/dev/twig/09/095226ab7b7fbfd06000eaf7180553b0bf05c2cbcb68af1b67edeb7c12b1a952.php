<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_03ad3e34f598989ba5e54eefe89312b367fa184ec3ab6c9bb87641b89a7557ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd9d25b82b088c0c2b7f4af79d1e555772be22d0a9e5f57e13ed4306cb370860 = $this->env->getExtension("native_profiler");
        $__internal_cd9d25b82b088c0c2b7f4af79d1e555772be22d0a9e5f57e13ed4306cb370860->enter($__internal_cd9d25b82b088c0c2b7f4af79d1e555772be22d0a9e5f57e13ed4306cb370860_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_cd9d25b82b088c0c2b7f4af79d1e555772be22d0a9e5f57e13ed4306cb370860->leave($__internal_cd9d25b82b088c0c2b7f4af79d1e555772be22d0a9e5f57e13ed4306cb370860_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
