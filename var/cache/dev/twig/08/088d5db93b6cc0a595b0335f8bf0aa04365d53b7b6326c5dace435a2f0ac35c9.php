<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_0ac922a225b795118f029fdfa9c1bcae202e6e7369cf52d04e85ac447a84a356 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e52ce1ddc8cd8ac269e3aa2d330e95958c1584ced13afa04c1a21be18806f98e = $this->env->getExtension("native_profiler");
        $__internal_e52ce1ddc8cd8ac269e3aa2d330e95958c1584ced13afa04c1a21be18806f98e->enter($__internal_e52ce1ddc8cd8ac269e3aa2d330e95958c1584ced13afa04c1a21be18806f98e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_e52ce1ddc8cd8ac269e3aa2d330e95958c1584ced13afa04c1a21be18806f98e->leave($__internal_e52ce1ddc8cd8ac269e3aa2d330e95958c1584ced13afa04c1a21be18806f98e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
