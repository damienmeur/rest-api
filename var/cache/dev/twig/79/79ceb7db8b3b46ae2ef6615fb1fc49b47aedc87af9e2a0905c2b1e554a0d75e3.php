<?php

/* @Twig/Exception/exception.css.twig */
class __TwigTemplate_903a82071f54663dd602dc962b804a6e46d35723263557de0b31343c51777f2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7480fd521caf22c877a8849431683b0c2a8582fe909181e4d1ac44e644a435a5 = $this->env->getExtension("native_profiler");
        $__internal_7480fd521caf22c877a8849431683b0c2a8582fe909181e4d1ac44e644a435a5->enter($__internal_7480fd521caf22c877a8849431683b0c2a8582fe909181e4d1ac44e644a435a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_7480fd521caf22c877a8849431683b0c2a8582fe909181e4d1ac44e644a435a5->leave($__internal_7480fd521caf22c877a8849431683b0c2a8582fe909181e4d1ac44e644a435a5_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
