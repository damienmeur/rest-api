<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_7dc897ad2d49b7501e5350ecee1f80d734b916e293bef42f041df9255435c114 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9c47e0c1a19b62abc80373fb9a2ddb5880937411b16abbe5503714478fcf001 = $this->env->getExtension("native_profiler");
        $__internal_a9c47e0c1a19b62abc80373fb9a2ddb5880937411b16abbe5503714478fcf001->enter($__internal_a9c47e0c1a19b62abc80373fb9a2ddb5880937411b16abbe5503714478fcf001_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_a9c47e0c1a19b62abc80373fb9a2ddb5880937411b16abbe5503714478fcf001->leave($__internal_a9c47e0c1a19b62abc80373fb9a2ddb5880937411b16abbe5503714478fcf001_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
