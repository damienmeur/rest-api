<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_731ea6433b8fe3044076d812bf7a09573c145bc35313c6a41ddc6fc13c0ef59b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9adb2ba8449c923f8754c97b342e8147dcb2cabb2c6d6782ff55d7e68bf77895 = $this->env->getExtension("native_profiler");
        $__internal_9adb2ba8449c923f8754c97b342e8147dcb2cabb2c6d6782ff55d7e68bf77895->enter($__internal_9adb2ba8449c923f8754c97b342e8147dcb2cabb2c6d6782ff55d7e68bf77895_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_9adb2ba8449c923f8754c97b342e8147dcb2cabb2c6d6782ff55d7e68bf77895->leave($__internal_9adb2ba8449c923f8754c97b342e8147dcb2cabb2c6d6782ff55d7e68bf77895_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
