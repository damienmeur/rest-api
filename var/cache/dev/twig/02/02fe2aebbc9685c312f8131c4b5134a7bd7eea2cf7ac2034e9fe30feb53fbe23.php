<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_434b67c194b5ff2000cb466ffa9467239800f0a7becb8af7e05c2f2b1a1bd318 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c64f6a8465dcea7fef397e9842320d91858b7a61c081fd44b7f5c61cd9e09788 = $this->env->getExtension("native_profiler");
        $__internal_c64f6a8465dcea7fef397e9842320d91858b7a61c081fd44b7f5c61cd9e09788->enter($__internal_c64f6a8465dcea7fef397e9842320d91858b7a61c081fd44b7f5c61cd9e09788_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_c64f6a8465dcea7fef397e9842320d91858b7a61c081fd44b7f5c61cd9e09788->leave($__internal_c64f6a8465dcea7fef397e9842320d91858b7a61c081fd44b7f5c61cd9e09788_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
