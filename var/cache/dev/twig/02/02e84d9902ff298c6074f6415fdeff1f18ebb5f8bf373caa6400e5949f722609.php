<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_02c53c81d8a6c1b36d79ab25ac3a5d3a13d4835ca4627b244fedb1c12a82f62a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cfa5713d8d8414447242916e8e3cabef986c5cd9745c4027dc7957a6cb3ce1b3 = $this->env->getExtension("native_profiler");
        $__internal_cfa5713d8d8414447242916e8e3cabef986c5cd9745c4027dc7957a6cb3ce1b3->enter($__internal_cfa5713d8d8414447242916e8e3cabef986c5cd9745c4027dc7957a6cb3ce1b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_cfa5713d8d8414447242916e8e3cabef986c5cd9745c4027dc7957a6cb3ce1b3->leave($__internal_cfa5713d8d8414447242916e8e3cabef986c5cd9745c4027dc7957a6cb3ce1b3_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
