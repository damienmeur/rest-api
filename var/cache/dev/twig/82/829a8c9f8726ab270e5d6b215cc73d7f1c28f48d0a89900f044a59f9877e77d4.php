<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_fa847ac155635a2053595b08973d038557a41cefade29c9af6792fea3233e15b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_50778efbd290e2e474059bf392900da2a401d2ca717f44156bcfdfd2c16f170f = $this->env->getExtension("native_profiler");
        $__internal_50778efbd290e2e474059bf392900da2a401d2ca717f44156bcfdfd2c16f170f->enter($__internal_50778efbd290e2e474059bf392900da2a401d2ca717f44156bcfdfd2c16f170f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_50778efbd290e2e474059bf392900da2a401d2ca717f44156bcfdfd2c16f170f->leave($__internal_50778efbd290e2e474059bf392900da2a401d2ca717f44156bcfdfd2c16f170f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
