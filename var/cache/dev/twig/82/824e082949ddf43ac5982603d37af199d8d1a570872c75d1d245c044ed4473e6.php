<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_02ff029cde970d2ba3ec104ecb6a7169e4a28c1fcda46659d33e287b9a0299ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93f2026e2c6830c038a2d63c44bac584fcd6c1055b1d70b0db4cdd1ea4a065bd = $this->env->getExtension("native_profiler");
        $__internal_93f2026e2c6830c038a2d63c44bac584fcd6c1055b1d70b0db4cdd1ea4a065bd->enter($__internal_93f2026e2c6830c038a2d63c44bac584fcd6c1055b1d70b0db4cdd1ea4a065bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_93f2026e2c6830c038a2d63c44bac584fcd6c1055b1d70b0db4cdd1ea4a065bd->leave($__internal_93f2026e2c6830c038a2d63c44bac584fcd6c1055b1d70b0db4cdd1ea4a065bd_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_79527226432aae5fd8620d79fa43392c706057a7b61789f077892d01514c32ca = $this->env->getExtension("native_profiler");
        $__internal_79527226432aae5fd8620d79fa43392c706057a7b61789f077892d01514c32ca->enter($__internal_79527226432aae5fd8620d79fa43392c706057a7b61789f077892d01514c32ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_79527226432aae5fd8620d79fa43392c706057a7b61789f077892d01514c32ca->leave($__internal_79527226432aae5fd8620d79fa43392c706057a7b61789f077892d01514c32ca_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_0da1b080b8152babd94f7231da2f467af95bf5ae43e47fbf6e8832285a07df32 = $this->env->getExtension("native_profiler");
        $__internal_0da1b080b8152babd94f7231da2f467af95bf5ae43e47fbf6e8832285a07df32->enter($__internal_0da1b080b8152babd94f7231da2f467af95bf5ae43e47fbf6e8832285a07df32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_0da1b080b8152babd94f7231da2f467af95bf5ae43e47fbf6e8832285a07df32->leave($__internal_0da1b080b8152babd94f7231da2f467af95bf5ae43e47fbf6e8832285a07df32_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
