<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_d7c86aae47505dd6da9bd073cbf07c54ca182826e0e3ff8d79ac8bcde9a6da85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b920940152fb2d0be98cd38af7d190c0f8e62a84ef289f3f9336f3b6275a8664 = $this->env->getExtension("native_profiler");
        $__internal_b920940152fb2d0be98cd38af7d190c0f8e62a84ef289f3f9336f3b6275a8664->enter($__internal_b920940152fb2d0be98cd38af7d190c0f8e62a84ef289f3f9336f3b6275a8664_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_b920940152fb2d0be98cd38af7d190c0f8e62a84ef289f3f9336f3b6275a8664->leave($__internal_b920940152fb2d0be98cd38af7d190c0f8e62a84ef289f3f9336f3b6275a8664_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
