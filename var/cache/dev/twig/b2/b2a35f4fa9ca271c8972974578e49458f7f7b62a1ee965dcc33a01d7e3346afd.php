<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_24b99f699ed5a39621df28e72b8261c356cd64311b524fb151d56a2a10fb3808 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4ca85a4c29aa2b05fa3309ec428c141412efcce0f32a8b8e04e01a5c43c88ce7 = $this->env->getExtension("native_profiler");
        $__internal_4ca85a4c29aa2b05fa3309ec428c141412efcce0f32a8b8e04e01a5c43c88ce7->enter($__internal_4ca85a4c29aa2b05fa3309ec428c141412efcce0f32a8b8e04e01a5c43c88ce7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_4ca85a4c29aa2b05fa3309ec428c141412efcce0f32a8b8e04e01a5c43c88ce7->leave($__internal_4ca85a4c29aa2b05fa3309ec428c141412efcce0f32a8b8e04e01a5c43c88ce7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
