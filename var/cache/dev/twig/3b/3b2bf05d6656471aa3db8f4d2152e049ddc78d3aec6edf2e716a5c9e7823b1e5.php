<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_6a9baf9ff52fe9a89a037cfb91105c22017b14f3a6be9b0a7626079636159bf1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a643fc84f7351d9e216ff7a8e4f90aa9abc37c2dc2fb296f0d88b6a45c9853ef = $this->env->getExtension("native_profiler");
        $__internal_a643fc84f7351d9e216ff7a8e4f90aa9abc37c2dc2fb296f0d88b6a45c9853ef->enter($__internal_a643fc84f7351d9e216ff7a8e4f90aa9abc37c2dc2fb296f0d88b6a45c9853ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_a643fc84f7351d9e216ff7a8e4f90aa9abc37c2dc2fb296f0d88b6a45c9853ef->leave($__internal_a643fc84f7351d9e216ff7a8e4f90aa9abc37c2dc2fb296f0d88b6a45c9853ef_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_rows') ?>*/
/* */
