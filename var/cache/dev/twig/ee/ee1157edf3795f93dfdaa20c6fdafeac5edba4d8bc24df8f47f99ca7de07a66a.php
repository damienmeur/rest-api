<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_1bb9a9b515fcbe65122bfef265ffdf929384066f5b9999f17bffe414b052c04c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fb197b7285c766c330990b5a25cfbc0c2a4c37cca932386b4b1848fec297f833 = $this->env->getExtension("native_profiler");
        $__internal_fb197b7285c766c330990b5a25cfbc0c2a4c37cca932386b4b1848fec297f833->enter($__internal_fb197b7285c766c330990b5a25cfbc0c2a4c37cca932386b4b1848fec297f833_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_fb197b7285c766c330990b5a25cfbc0c2a4c37cca932386b4b1848fec297f833->leave($__internal_fb197b7285c766c330990b5a25cfbc0c2a4c37cca932386b4b1848fec297f833_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
