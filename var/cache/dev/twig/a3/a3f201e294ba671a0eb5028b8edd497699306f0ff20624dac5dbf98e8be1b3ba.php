<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_1e0ab0f127d3e871c609a1da8e5af46e40be07edff60b011bec8356d5d98dda8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_684a88f4416e354778d35b96859d995f104fcdbcbcec8421ed9f4bb932baf42a = $this->env->getExtension("native_profiler");
        $__internal_684a88f4416e354778d35b96859d995f104fcdbcbcec8421ed9f4bb932baf42a->enter($__internal_684a88f4416e354778d35b96859d995f104fcdbcbcec8421ed9f4bb932baf42a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_684a88f4416e354778d35b96859d995f104fcdbcbcec8421ed9f4bb932baf42a->leave($__internal_684a88f4416e354778d35b96859d995f104fcdbcbcec8421ed9f4bb932baf42a_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_cb93ef8a37fde5f369a7e7ecd6656d96875d191a589179981ba6e8add060b532 = $this->env->getExtension("native_profiler");
        $__internal_cb93ef8a37fde5f369a7e7ecd6656d96875d191a589179981ba6e8add060b532->enter($__internal_cb93ef8a37fde5f369a7e7ecd6656d96875d191a589179981ba6e8add060b532_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_cb93ef8a37fde5f369a7e7ecd6656d96875d191a589179981ba6e8add060b532->leave($__internal_cb93ef8a37fde5f369a7e7ecd6656d96875d191a589179981ba6e8add060b532_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_cb00e40305dc19f21fb35e8f355e22dc54e094d1d8a9fc6cbf673689e3795606 = $this->env->getExtension("native_profiler");
        $__internal_cb00e40305dc19f21fb35e8f355e22dc54e094d1d8a9fc6cbf673689e3795606->enter($__internal_cb00e40305dc19f21fb35e8f355e22dc54e094d1d8a9fc6cbf673689e3795606_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_cb00e40305dc19f21fb35e8f355e22dc54e094d1d8a9fc6cbf673689e3795606->leave($__internal_cb00e40305dc19f21fb35e8f355e22dc54e094d1d8a9fc6cbf673689e3795606_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_6e867103ec051510528cd945ca414d269e4c589edbdc45134cb7540f7aa16d95 = $this->env->getExtension("native_profiler");
        $__internal_6e867103ec051510528cd945ca414d269e4c589edbdc45134cb7540f7aa16d95->enter($__internal_6e867103ec051510528cd945ca414d269e4c589edbdc45134cb7540f7aa16d95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_6e867103ec051510528cd945ca414d269e4c589edbdc45134cb7540f7aa16d95->leave($__internal_6e867103ec051510528cd945ca414d269e4c589edbdc45134cb7540f7aa16d95_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
