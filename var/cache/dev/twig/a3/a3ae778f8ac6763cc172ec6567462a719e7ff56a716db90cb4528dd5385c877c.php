<?php

/* @Twig/Exception/error.xml.twig */
class __TwigTemplate_de538523ac17ebb3b5ee8b009b8a65461d8671e666e7eef4dece1e7889c2e06a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0fcd0a99bc94a90f103b5335267268490f3f90d9b7b61a9c2408fcde3423e7c5 = $this->env->getExtension("native_profiler");
        $__internal_0fcd0a99bc94a90f103b5335267268490f3f90d9b7b61a9c2408fcde3423e7c5->enter($__internal_0fcd0a99bc94a90f103b5335267268490f3f90d9b7b61a9c2408fcde3423e7c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_0fcd0a99bc94a90f103b5335267268490f3f90d9b7b61a9c2408fcde3423e7c5->leave($__internal_0fcd0a99bc94a90f103b5335267268490f3f90d9b7b61a9c2408fcde3423e7c5_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
