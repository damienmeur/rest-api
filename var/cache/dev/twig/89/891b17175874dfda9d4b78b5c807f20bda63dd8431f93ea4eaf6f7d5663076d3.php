<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_9a5997da165b55e7dd4e03cb6c5f5fdb2a16b2d986860f9682f2ee88731e9471 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6a8339337f65eed8033ec5451fa2c510435a35bdf9d4f5b5e2229e1b04973d75 = $this->env->getExtension("native_profiler");
        $__internal_6a8339337f65eed8033ec5451fa2c510435a35bdf9d4f5b5e2229e1b04973d75->enter($__internal_6a8339337f65eed8033ec5451fa2c510435a35bdf9d4f5b5e2229e1b04973d75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_6a8339337f65eed8033ec5451fa2c510435a35bdf9d4f5b5e2229e1b04973d75->leave($__internal_6a8339337f65eed8033ec5451fa2c510435a35bdf9d4f5b5e2229e1b04973d75_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
