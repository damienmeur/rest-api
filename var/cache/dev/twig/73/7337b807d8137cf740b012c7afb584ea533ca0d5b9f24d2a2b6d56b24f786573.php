<?php

/* base.html.twig */
class __TwigTemplate_d0b4161a950ba78eda1ee12c9317e05c9603c7e9686be08b80e86e5a25383418 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ddbfaf3b518dedd995db31f0fc6bed3bbb1e62eb0e38d74a9aedba57644381a = $this->env->getExtension("native_profiler");
        $__internal_1ddbfaf3b518dedd995db31f0fc6bed3bbb1e62eb0e38d74a9aedba57644381a->enter($__internal_1ddbfaf3b518dedd995db31f0fc6bed3bbb1e62eb0e38d74a9aedba57644381a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_1ddbfaf3b518dedd995db31f0fc6bed3bbb1e62eb0e38d74a9aedba57644381a->leave($__internal_1ddbfaf3b518dedd995db31f0fc6bed3bbb1e62eb0e38d74a9aedba57644381a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_9f540b316fc5b393850c357bf23bff980cdb888a4d4bcfc0b661be5877d305f7 = $this->env->getExtension("native_profiler");
        $__internal_9f540b316fc5b393850c357bf23bff980cdb888a4d4bcfc0b661be5877d305f7->enter($__internal_9f540b316fc5b393850c357bf23bff980cdb888a4d4bcfc0b661be5877d305f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_9f540b316fc5b393850c357bf23bff980cdb888a4d4bcfc0b661be5877d305f7->leave($__internal_9f540b316fc5b393850c357bf23bff980cdb888a4d4bcfc0b661be5877d305f7_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9c8367088f1444e6b144384ff8f71b994f914757eb17730ceb9f23527b7ca63b = $this->env->getExtension("native_profiler");
        $__internal_9c8367088f1444e6b144384ff8f71b994f914757eb17730ceb9f23527b7ca63b->enter($__internal_9c8367088f1444e6b144384ff8f71b994f914757eb17730ceb9f23527b7ca63b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_9c8367088f1444e6b144384ff8f71b994f914757eb17730ceb9f23527b7ca63b->leave($__internal_9c8367088f1444e6b144384ff8f71b994f914757eb17730ceb9f23527b7ca63b_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_c02e31d8c2c3dfc5d9b90be8dd3f0c07466482a00ad9d75a832e8268c638d236 = $this->env->getExtension("native_profiler");
        $__internal_c02e31d8c2c3dfc5d9b90be8dd3f0c07466482a00ad9d75a832e8268c638d236->enter($__internal_c02e31d8c2c3dfc5d9b90be8dd3f0c07466482a00ad9d75a832e8268c638d236_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_c02e31d8c2c3dfc5d9b90be8dd3f0c07466482a00ad9d75a832e8268c638d236->leave($__internal_c02e31d8c2c3dfc5d9b90be8dd3f0c07466482a00ad9d75a832e8268c638d236_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_fa56f3e89bee4d7ee380dc8a80529e2e3f368bed00f5a912c780e279f4d17abb = $this->env->getExtension("native_profiler");
        $__internal_fa56f3e89bee4d7ee380dc8a80529e2e3f368bed00f5a912c780e279f4d17abb->enter($__internal_fa56f3e89bee4d7ee380dc8a80529e2e3f368bed00f5a912c780e279f4d17abb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_fa56f3e89bee4d7ee380dc8a80529e2e3f368bed00f5a912c780e279f4d17abb->leave($__internal_fa56f3e89bee4d7ee380dc8a80529e2e3f368bed00f5a912c780e279f4d17abb_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
