<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_85638581afdcad195d223b5f93f088e828d6d5d356f9dd97636531d09e4797cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2cd72ec265c8fe1146f3390c9fc3f27a8cd167bed80201c69dded4cc83eb7eb0 = $this->env->getExtension("native_profiler");
        $__internal_2cd72ec265c8fe1146f3390c9fc3f27a8cd167bed80201c69dded4cc83eb7eb0->enter($__internal_2cd72ec265c8fe1146f3390c9fc3f27a8cd167bed80201c69dded4cc83eb7eb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_2cd72ec265c8fe1146f3390c9fc3f27a8cd167bed80201c69dded4cc83eb7eb0->leave($__internal_2cd72ec265c8fe1146f3390c9fc3f27a8cd167bed80201c69dded4cc83eb7eb0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
