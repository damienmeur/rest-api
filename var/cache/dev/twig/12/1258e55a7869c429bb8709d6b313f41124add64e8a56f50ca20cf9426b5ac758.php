<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_6d70e711737abcccea2eee85110082e70c5d9f5f4cbb8b12caebd126e9dc1b57 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a7dea4fc196e00e6f6ddb8877ead44daecc1dcc43950a892bd9aaaeab1d343f = $this->env->getExtension("native_profiler");
        $__internal_4a7dea4fc196e00e6f6ddb8877ead44daecc1dcc43950a892bd9aaaeab1d343f->enter($__internal_4a7dea4fc196e00e6f6ddb8877ead44daecc1dcc43950a892bd9aaaeab1d343f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_4a7dea4fc196e00e6f6ddb8877ead44daecc1dcc43950a892bd9aaaeab1d343f->leave($__internal_4a7dea4fc196e00e6f6ddb8877ead44daecc1dcc43950a892bd9aaaeab1d343f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
