<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_b8b762ad1dc7bebf5d4697f78ab62bc5924b6be5d77a7bea23d88baabc45a2ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21e283b08c6894ef2e57087e77655c330915e920639691ec387daeb1c65b1265 = $this->env->getExtension("native_profiler");
        $__internal_21e283b08c6894ef2e57087e77655c330915e920639691ec387daeb1c65b1265->enter($__internal_21e283b08c6894ef2e57087e77655c330915e920639691ec387daeb1c65b1265_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_21e283b08c6894ef2e57087e77655c330915e920639691ec387daeb1c65b1265->leave($__internal_21e283b08c6894ef2e57087e77655c330915e920639691ec387daeb1c65b1265_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
