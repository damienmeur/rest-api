<?php

/* @Twig/Exception/error.css.twig */
class __TwigTemplate_79c129b0fd5492cc74f686f134f694ee9fd0d4d08d1b04431d3bc6ac4ead9946 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_90f4308ee03dd545656479ef09124be044d0444e7d27887708353209c74c2cd4 = $this->env->getExtension("native_profiler");
        $__internal_90f4308ee03dd545656479ef09124be044d0444e7d27887708353209c74c2cd4->enter($__internal_90f4308ee03dd545656479ef09124be044d0444e7d27887708353209c74c2cd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_90f4308ee03dd545656479ef09124be044d0444e7d27887708353209c74c2cd4->leave($__internal_90f4308ee03dd545656479ef09124be044d0444e7d27887708353209c74c2cd4_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
