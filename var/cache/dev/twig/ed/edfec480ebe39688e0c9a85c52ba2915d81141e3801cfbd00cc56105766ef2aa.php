<?php

/* @Twig/Exception/error.js.twig */
class __TwigTemplate_8e9f5db451d14d05aa865355fd22844f097139d8341abf907b0b305429e1d246 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e5ad5b103e5924870cec85aefbf632ccd8ab6e7be0c99da1f24abfd10dc82d2 = $this->env->getExtension("native_profiler");
        $__internal_8e5ad5b103e5924870cec85aefbf632ccd8ab6e7be0c99da1f24abfd10dc82d2->enter($__internal_8e5ad5b103e5924870cec85aefbf632ccd8ab6e7be0c99da1f24abfd10dc82d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_8e5ad5b103e5924870cec85aefbf632ccd8ab6e7be0c99da1f24abfd10dc82d2->leave($__internal_8e5ad5b103e5924870cec85aefbf632ccd8ab6e7be0c99da1f24abfd10dc82d2_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
