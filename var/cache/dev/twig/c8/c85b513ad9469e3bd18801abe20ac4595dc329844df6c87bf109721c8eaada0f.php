<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_79a852ef2372f5a8aebee76452b1b9958ce8eb962e1660de56a4229d30e315cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c2ba25f3a2b275cf30b6a5ba614abf7378e23f073ce44f1377cbe2a3b0fa240a = $this->env->getExtension("native_profiler");
        $__internal_c2ba25f3a2b275cf30b6a5ba614abf7378e23f073ce44f1377cbe2a3b0fa240a->enter($__internal_c2ba25f3a2b275cf30b6a5ba614abf7378e23f073ce44f1377cbe2a3b0fa240a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_c2ba25f3a2b275cf30b6a5ba614abf7378e23f073ce44f1377cbe2a3b0fa240a->leave($__internal_c2ba25f3a2b275cf30b6a5ba614abf7378e23f073ce44f1377cbe2a3b0fa240a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
