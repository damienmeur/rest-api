<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_7644a92900990e3087753632b3fc477e1cf1aa690c975bcc60bf64574169ce85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4e64ebdaed7ee716fe865c5b1e274fcc0bdba8321314a750206c19b039ed50f0 = $this->env->getExtension("native_profiler");
        $__internal_4e64ebdaed7ee716fe865c5b1e274fcc0bdba8321314a750206c19b039ed50f0->enter($__internal_4e64ebdaed7ee716fe865c5b1e274fcc0bdba8321314a750206c19b039ed50f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4e64ebdaed7ee716fe865c5b1e274fcc0bdba8321314a750206c19b039ed50f0->leave($__internal_4e64ebdaed7ee716fe865c5b1e274fcc0bdba8321314a750206c19b039ed50f0_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_15854c9fbb7b8bb7896963a1b93bc3a33dd606d2de42832145cc392522e570dc = $this->env->getExtension("native_profiler");
        $__internal_15854c9fbb7b8bb7896963a1b93bc3a33dd606d2de42832145cc392522e570dc->enter($__internal_15854c9fbb7b8bb7896963a1b93bc3a33dd606d2de42832145cc392522e570dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_15854c9fbb7b8bb7896963a1b93bc3a33dd606d2de42832145cc392522e570dc->leave($__internal_15854c9fbb7b8bb7896963a1b93bc3a33dd606d2de42832145cc392522e570dc_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_4616f511ddc0cf88f28f87a3ff03e028dc5a44db84876f1e466a158eb7b17077 = $this->env->getExtension("native_profiler");
        $__internal_4616f511ddc0cf88f28f87a3ff03e028dc5a44db84876f1e466a158eb7b17077->enter($__internal_4616f511ddc0cf88f28f87a3ff03e028dc5a44db84876f1e466a158eb7b17077_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_4616f511ddc0cf88f28f87a3ff03e028dc5a44db84876f1e466a158eb7b17077->leave($__internal_4616f511ddc0cf88f28f87a3ff03e028dc5a44db84876f1e466a158eb7b17077_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5641a7bd751ed9dd0034999038fd1fa4f5e1fb99a4b864f20f3ccb682528c822 = $this->env->getExtension("native_profiler");
        $__internal_5641a7bd751ed9dd0034999038fd1fa4f5e1fb99a4b864f20f3ccb682528c822->enter($__internal_5641a7bd751ed9dd0034999038fd1fa4f5e1fb99a4b864f20f3ccb682528c822_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_5641a7bd751ed9dd0034999038fd1fa4f5e1fb99a4b864f20f3ccb682528c822->leave($__internal_5641a7bd751ed9dd0034999038fd1fa4f5e1fb99a4b864f20f3ccb682528c822_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
