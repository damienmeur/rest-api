<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_3031776a86320547e282f75a63c2aab5f4557f863a836cc924f92e5a84edd274 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0bfa3ef3022b252c8ba13d4e514205b81f543a44b9fe3962f00d9808a7aefe58 = $this->env->getExtension("native_profiler");
        $__internal_0bfa3ef3022b252c8ba13d4e514205b81f543a44b9fe3962f00d9808a7aefe58->enter($__internal_0bfa3ef3022b252c8ba13d4e514205b81f543a44b9fe3962f00d9808a7aefe58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_0bfa3ef3022b252c8ba13d4e514205b81f543a44b9fe3962f00d9808a7aefe58->leave($__internal_0bfa3ef3022b252c8ba13d4e514205b81f543a44b9fe3962f00d9808a7aefe58_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
