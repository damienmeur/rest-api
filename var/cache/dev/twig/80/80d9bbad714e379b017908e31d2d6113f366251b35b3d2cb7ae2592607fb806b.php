<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_692e0c7d4f452d6c7026427c797b5f6b7c454300a46d4ed3e723174737fdb575 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_315e265316e0b1fef66021631e248560237e1d2aad665323ecf6f86222592caf = $this->env->getExtension("native_profiler");
        $__internal_315e265316e0b1fef66021631e248560237e1d2aad665323ecf6f86222592caf->enter($__internal_315e265316e0b1fef66021631e248560237e1d2aad665323ecf6f86222592caf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_315e265316e0b1fef66021631e248560237e1d2aad665323ecf6f86222592caf->leave($__internal_315e265316e0b1fef66021631e248560237e1d2aad665323ecf6f86222592caf_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
