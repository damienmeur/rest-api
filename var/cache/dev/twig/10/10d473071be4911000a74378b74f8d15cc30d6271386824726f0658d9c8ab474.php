<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_c6984b501c99706a7ae63825da577f97121bdda08e7a09fbcfe2bc37d2ee24a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4459d45a4a6c38950cb3829706d680e0482df7d06bad260bf01c1a7140b32617 = $this->env->getExtension("native_profiler");
        $__internal_4459d45a4a6c38950cb3829706d680e0482df7d06bad260bf01c1a7140b32617->enter($__internal_4459d45a4a6c38950cb3829706d680e0482df7d06bad260bf01c1a7140b32617_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_4459d45a4a6c38950cb3829706d680e0482df7d06bad260bf01c1a7140b32617->leave($__internal_4459d45a4a6c38950cb3829706d680e0482df7d06bad260bf01c1a7140b32617_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
