<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_8a62d04140e69a098e9ba7133bef29c9b979c75529260b634430a657792922a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5638aeb392f637b5b549c6f63e904eb5a9f909ac6103e31cba4c4207f86d5139 = $this->env->getExtension("native_profiler");
        $__internal_5638aeb392f637b5b549c6f63e904eb5a9f909ac6103e31cba4c4207f86d5139->enter($__internal_5638aeb392f637b5b549c6f63e904eb5a9f909ac6103e31cba4c4207f86d5139_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_5638aeb392f637b5b549c6f63e904eb5a9f909ac6103e31cba4c4207f86d5139->leave($__internal_5638aeb392f637b5b549c6f63e904eb5a9f909ac6103e31cba4c4207f86d5139_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
