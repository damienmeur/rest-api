<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_8d0591a483fda4c12b8149e67fdf2de9aacae4186a346b993954808a18a1579b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e742f42423b310310136a8bebd32e60a75ffb23c4192ab7fcc147cc778ca067b = $this->env->getExtension("native_profiler");
        $__internal_e742f42423b310310136a8bebd32e60a75ffb23c4192ab7fcc147cc778ca067b->enter($__internal_e742f42423b310310136a8bebd32e60a75ffb23c4192ab7fcc147cc778ca067b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_e742f42423b310310136a8bebd32e60a75ffb23c4192ab7fcc147cc778ca067b->leave($__internal_e742f42423b310310136a8bebd32e60a75ffb23c4192ab7fcc147cc778ca067b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
