<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_6c1a4403575c983d7707bca743f02c2ed3c2a759a304b71e5a84c86915525d76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d0cf90c4f7c26d544aa4ba6f3918b4dfa01f08a447fe7ae5e110be29310ccfd = $this->env->getExtension("native_profiler");
        $__internal_1d0cf90c4f7c26d544aa4ba6f3918b4dfa01f08a447fe7ae5e110be29310ccfd->enter($__internal_1d0cf90c4f7c26d544aa4ba6f3918b4dfa01f08a447fe7ae5e110be29310ccfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_1d0cf90c4f7c26d544aa4ba6f3918b4dfa01f08a447fe7ae5e110be29310ccfd->leave($__internal_1d0cf90c4f7c26d544aa4ba6f3918b4dfa01f08a447fe7ae5e110be29310ccfd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
