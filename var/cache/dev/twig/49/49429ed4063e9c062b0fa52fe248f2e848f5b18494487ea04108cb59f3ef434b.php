<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_7b4f96a2eccbc83d73e8fcca8c152aa05edc81db6276fc38cd49080fab088e08 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d7f778399a9df0d6d0c02160a917ddd4fbe73a7483f60c4b36976a2bfb38f7e = $this->env->getExtension("native_profiler");
        $__internal_8d7f778399a9df0d6d0c02160a917ddd4fbe73a7483f60c4b36976a2bfb38f7e->enter($__internal_8d7f778399a9df0d6d0c02160a917ddd4fbe73a7483f60c4b36976a2bfb38f7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_8d7f778399a9df0d6d0c02160a917ddd4fbe73a7483f60c4b36976a2bfb38f7e->leave($__internal_8d7f778399a9df0d6d0c02160a917ddd4fbe73a7483f60c4b36976a2bfb38f7e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
