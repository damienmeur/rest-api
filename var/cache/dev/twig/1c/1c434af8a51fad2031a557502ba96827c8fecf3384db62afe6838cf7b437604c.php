<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_a4b81819c886f69b1dc6bec497132a0e5b51c5396849371506e122b30446bae8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_845fad7dc904aec1a5c6d2a3893644366cfb317b1ff35e637b734e79c224e9ba = $this->env->getExtension("native_profiler");
        $__internal_845fad7dc904aec1a5c6d2a3893644366cfb317b1ff35e637b734e79c224e9ba->enter($__internal_845fad7dc904aec1a5c6d2a3893644366cfb317b1ff35e637b734e79c224e9ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_845fad7dc904aec1a5c6d2a3893644366cfb317b1ff35e637b734e79c224e9ba->leave($__internal_845fad7dc904aec1a5c6d2a3893644366cfb317b1ff35e637b734e79c224e9ba_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
