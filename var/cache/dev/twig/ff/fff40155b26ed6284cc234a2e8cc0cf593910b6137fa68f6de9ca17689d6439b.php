<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_2ce7fc22b53c3231563d08c36991e7542f37d4d1e8f306febfe730729db6693c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6642fb5e78ec86b60efb24a8c7c2a84b20b28ae4c2f8c392beac85d8b37c4b1a = $this->env->getExtension("native_profiler");
        $__internal_6642fb5e78ec86b60efb24a8c7c2a84b20b28ae4c2f8c392beac85d8b37c4b1a->enter($__internal_6642fb5e78ec86b60efb24a8c7c2a84b20b28ae4c2f8c392beac85d8b37c4b1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_6642fb5e78ec86b60efb24a8c7c2a84b20b28ae4c2f8c392beac85d8b37c4b1a->leave($__internal_6642fb5e78ec86b60efb24a8c7c2a84b20b28ae4c2f8c392beac85d8b37c4b1a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
