<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_c78fdb5303f35927b319403489a32afc9dfd7dd1c38f7cf071da8505db14a437 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_014bcad91f2ba3b408c18b2ed2b29c21b61896de9b651d1154e603aee1e1ac2d = $this->env->getExtension("native_profiler");
        $__internal_014bcad91f2ba3b408c18b2ed2b29c21b61896de9b651d1154e603aee1e1ac2d->enter($__internal_014bcad91f2ba3b408c18b2ed2b29c21b61896de9b651d1154e603aee1e1ac2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_014bcad91f2ba3b408c18b2ed2b29c21b61896de9b651d1154e603aee1e1ac2d->leave($__internal_014bcad91f2ba3b408c18b2ed2b29c21b61896de9b651d1154e603aee1e1ac2d_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_50a69f91bb4be0f168d0b223077c59ddaa4c1afe973497054b7b2e68812f79db = $this->env->getExtension("native_profiler");
        $__internal_50a69f91bb4be0f168d0b223077c59ddaa4c1afe973497054b7b2e68812f79db->enter($__internal_50a69f91bb4be0f168d0b223077c59ddaa4c1afe973497054b7b2e68812f79db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_50a69f91bb4be0f168d0b223077c59ddaa4c1afe973497054b7b2e68812f79db->leave($__internal_50a69f91bb4be0f168d0b223077c59ddaa4c1afe973497054b7b2e68812f79db_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_97c7dee3569000cfc12bdefc729dfd85b930c61e0e4fe9220ae841cf069cf32c = $this->env->getExtension("native_profiler");
        $__internal_97c7dee3569000cfc12bdefc729dfd85b930c61e0e4fe9220ae841cf069cf32c->enter($__internal_97c7dee3569000cfc12bdefc729dfd85b930c61e0e4fe9220ae841cf069cf32c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_97c7dee3569000cfc12bdefc729dfd85b930c61e0e4fe9220ae841cf069cf32c->leave($__internal_97c7dee3569000cfc12bdefc729dfd85b930c61e0e4fe9220ae841cf069cf32c_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
