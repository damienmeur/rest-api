<?php

/* ::base.html.twig */
class __TwigTemplate_ec45f5b88cbb57eda3dbc4d4e34592fc9d4f2364a81d1ec30d936c7aaa0cc403 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b0bb1117b363b89715fa0275a8309c83d6d234a54be900072fc780f42c1e44b4 = $this->env->getExtension("native_profiler");
        $__internal_b0bb1117b363b89715fa0275a8309c83d6d234a54be900072fc780f42c1e44b4->enter($__internal_b0bb1117b363b89715fa0275a8309c83d6d234a54be900072fc780f42c1e44b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_b0bb1117b363b89715fa0275a8309c83d6d234a54be900072fc780f42c1e44b4->leave($__internal_b0bb1117b363b89715fa0275a8309c83d6d234a54be900072fc780f42c1e44b4_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_a97c5c7694a6db0837c72a38e44b9bda74f4fb2f5c35d646365f602736630780 = $this->env->getExtension("native_profiler");
        $__internal_a97c5c7694a6db0837c72a38e44b9bda74f4fb2f5c35d646365f602736630780->enter($__internal_a97c5c7694a6db0837c72a38e44b9bda74f4fb2f5c35d646365f602736630780_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_a97c5c7694a6db0837c72a38e44b9bda74f4fb2f5c35d646365f602736630780->leave($__internal_a97c5c7694a6db0837c72a38e44b9bda74f4fb2f5c35d646365f602736630780_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6f4e3413afad9ec11bf2487b304cf593e84c31cd9df47dd30d2a13a6ca76c7dc = $this->env->getExtension("native_profiler");
        $__internal_6f4e3413afad9ec11bf2487b304cf593e84c31cd9df47dd30d2a13a6ca76c7dc->enter($__internal_6f4e3413afad9ec11bf2487b304cf593e84c31cd9df47dd30d2a13a6ca76c7dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_6f4e3413afad9ec11bf2487b304cf593e84c31cd9df47dd30d2a13a6ca76c7dc->leave($__internal_6f4e3413afad9ec11bf2487b304cf593e84c31cd9df47dd30d2a13a6ca76c7dc_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_287835954dbc021dc7a1b7c4dda868c1f420c78480431428b5a52f59f85196b9 = $this->env->getExtension("native_profiler");
        $__internal_287835954dbc021dc7a1b7c4dda868c1f420c78480431428b5a52f59f85196b9->enter($__internal_287835954dbc021dc7a1b7c4dda868c1f420c78480431428b5a52f59f85196b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_287835954dbc021dc7a1b7c4dda868c1f420c78480431428b5a52f59f85196b9->leave($__internal_287835954dbc021dc7a1b7c4dda868c1f420c78480431428b5a52f59f85196b9_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_97f6e06b1f334441b99fd0766c0403987296c70a1500e3f49cd8f11e17aea7db = $this->env->getExtension("native_profiler");
        $__internal_97f6e06b1f334441b99fd0766c0403987296c70a1500e3f49cd8f11e17aea7db->enter($__internal_97f6e06b1f334441b99fd0766c0403987296c70a1500e3f49cd8f11e17aea7db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_97f6e06b1f334441b99fd0766c0403987296c70a1500e3f49cd8f11e17aea7db->leave($__internal_97f6e06b1f334441b99fd0766c0403987296c70a1500e3f49cd8f11e17aea7db_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
