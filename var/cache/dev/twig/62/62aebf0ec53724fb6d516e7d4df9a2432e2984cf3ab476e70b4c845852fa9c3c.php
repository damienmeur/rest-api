<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_f343f68520206ff564ea56a68d351ce48e2dc2220edc4bc633640eaca23caacc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_23bc5e21637196400cd3650d0830a35dbc149904377831d05ff73c32b5b40c96 = $this->env->getExtension("native_profiler");
        $__internal_23bc5e21637196400cd3650d0830a35dbc149904377831d05ff73c32b5b40c96->enter($__internal_23bc5e21637196400cd3650d0830a35dbc149904377831d05ff73c32b5b40c96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_23bc5e21637196400cd3650d0830a35dbc149904377831d05ff73c32b5b40c96->leave($__internal_23bc5e21637196400cd3650d0830a35dbc149904377831d05ff73c32b5b40c96_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
