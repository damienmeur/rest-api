<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_e40ebf9e985a906e706aee64d6974450c0b9730e09ad11b525ce5f8b6fd21be8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e2b00a315fbfc90c9817b88b4b6d63de5f7bb1196e83ea3cc1a32a609e6239a1 = $this->env->getExtension("native_profiler");
        $__internal_e2b00a315fbfc90c9817b88b4b6d63de5f7bb1196e83ea3cc1a32a609e6239a1->enter($__internal_e2b00a315fbfc90c9817b88b4b6d63de5f7bb1196e83ea3cc1a32a609e6239a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_e2b00a315fbfc90c9817b88b4b6d63de5f7bb1196e83ea3cc1a32a609e6239a1->leave($__internal_e2b00a315fbfc90c9817b88b4b6d63de5f7bb1196e83ea3cc1a32a609e6239a1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
