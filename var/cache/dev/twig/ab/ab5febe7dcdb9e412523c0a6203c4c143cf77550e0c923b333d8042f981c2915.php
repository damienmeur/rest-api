<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_8dd388bddf1d04b2fd6b9991f83b53e30420b5c9db4e4c457c4cb08a2e8a3659 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7973d43b6465858842e73ae2c3aeb7ca09cce2a60fc0f8665026796ac867a3b5 = $this->env->getExtension("native_profiler");
        $__internal_7973d43b6465858842e73ae2c3aeb7ca09cce2a60fc0f8665026796ac867a3b5->enter($__internal_7973d43b6465858842e73ae2c3aeb7ca09cce2a60fc0f8665026796ac867a3b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_7973d43b6465858842e73ae2c3aeb7ca09cce2a60fc0f8665026796ac867a3b5->leave($__internal_7973d43b6465858842e73ae2c3aeb7ca09cce2a60fc0f8665026796ac867a3b5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
